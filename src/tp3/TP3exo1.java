package tp3;

import java.util.Scanner;

public class TP3exo1 {

    public static void main(String[] args) {
        Scanner saisie = new Scanner(System.in);
        int nbnote = 0, stop = 0;
        float max = 0, min = 20, addition = 0, note = 0;
        
        while(stop == 0) {
            System.out.println("Veuillez saisir une note: ");
            note = saisie.nextFloat();
            if(note != -1) { addition = (float) (addition + note); }
            if(note > max) { max = (float) note; }
            if(note != -1 && note < min) { min = (float) note; }
            if(note == -1) {
                stop = 1;
            }
            else {
                nbnote++;
            }
        }
        System.out.println("Il y a " + nbnote + " note(s) saisie(s).");
        System.out.println("La moyenne des notes saisies est de " + addition / nbnote + ".");
        System.out.println("La note maximale est " + max + " et la minimale est "+ min +".");  
    }
}
