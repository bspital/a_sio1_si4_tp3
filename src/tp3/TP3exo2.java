package tp3;

import java.util.Random;
import java.util.Scanner;

public class TP3exo2 {

    public static void main(String[] args) {
        Scanner saisie = new Scanner(System.in);
        Random rd = new Random();
        int nbralea = rd.nextInt(1000)+1, nbr, stop = 0;
        
        System.out.println("Veuillez saisir un nombre entre 1 et 1000: ");
        while(stop == 0) {
            nbr = saisie.nextInt();
            if(nbr > nbralea) {
                System.out.println("C'est moins");
            }
            else if(nbr < nbralea) {
                System.out.println("C'est plus");
            }
            else {
                stop = 1;
            } 
        }
        System.out.println("Vous avez trouvé le bon nombre !");    
    }
}
