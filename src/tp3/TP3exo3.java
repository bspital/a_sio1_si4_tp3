package tp3;

import java.util.Random;
import java.util.Scanner;

public class TP3exo3 {

    public static void main(String[] args) {
        Scanner saisie = new Scanner(System.in);
        Random rd = new Random();
        int nbralea = rd.nextInt(1000)+1, nbr, stop = 0;
        
        System.out.println("Veuillez saisir un nombre entre 1 et 1000: (0 pour abandonner)");
        while(stop == 0) {
            nbr = saisie.nextInt();
            if(nbr == 0) { //abandon
                System.out.println("Vous avez décidé d'abandonner, le nombre à trouver était: "+nbralea);
            }
            else {
                if(nbr > nbralea) {
                    System.out.println("C'est moins");
                }
                else if(nbr < nbralea) {
                    System.out.println("C'est plus");
                }
                else {
                    System.out.println("Vous avez trouvé le bon nombre !");
                    stop = 1;
                } 
            }
        }
    }
}
